from flask import Flask, jsonify, render_template, request

import os
from time import sleep
from multiprocessing import Process
import configparser

from led_mode_executor import LedModeExecutor
from multi_color import MultiColorMode
from set_color_action import LedSingleColor
from set_color_delay_action import LedSetColorDelay
from single_color import SingleColorMode
from spotify_background_color import SpotifyBackgroundColor
from current_spotify_playback import CurrentSpotifyPlayback, NoArtworkException
from led_controller import LEDController



app = Flask(__name__)





@app.route('/')
def main():
    return render_template('index.html')


@app.route('/spotify')
def spotify():
    p = process
    if not p.is_alive():
        p = Process(target=main_spotify, args=())
        p.start()
    return render_template('spotify.html')


@app.route('/manual')
def manual():
    try:
        process.terminate()
    except AttributeError:
        pass
    return render_template('manual.html')


@app.route('/color', methods=['GET', 'POST'])
def color():
    if request.method == 'POST':
        data = request.json
        r = data['r']
        g = data['g']
        b = data['b']
        led.set_color(r, g, b, delay=0)

        return jsonify(status='updating', data=data)
    else:
        curr_r, curr_g, curr_b = led.get_color()
        return jsonify(status='current', data={'r': curr_r, 'g': curr_g, 'b': curr_b})


@app.route('/off')
def off():
    try:
        process.terminate()
    except AttributeError:
        pass
    led.set_color(0, 0, 0)
    return render_template('off.html')


def main_spotify():
    old_song_id = ''
    while True:
        spotify.update_current_playback()
        if spotify.connected_to_chromecast(name):
            if spotify.new_song(old_song_id):
                try:
                    artwork = spotify.get_artwork()
                    background_color = SpotifyBackgroundColor(
                        img=artwork, image_processing_size=(100, 100))
                    r, g, b = background_color.best_color(
                        k=8, color_tol=0)
                except NoArtworkException:
                    r, g, b = 255, 255, 255
                led.set_color(r, g, b)
                old_song_id = spotify.get_current_song_id()
        else:
            old_song_id = ''
            r, g, b = led.get_color()
            if r != 0 or g != 0 or b != 0:
                led.set_color(0, 0, 0)
        # executor.execute()
        sleep(2)



DEFAULT_COLOR = (210, 105, 30)

config = configparser.ConfigParser()
config.read('config.ini')
WS281X = config['WS281X']
if WS281X['is_active'] == 'True':
    print('WS281X is active...')
    print('WS281X is active creating controller')
    from ws281x_controller import WS281XController
    LED_COUNT = int(WS281X['led_count'])
    LED_PIN = int(WS281X['led_pin'])
    LED_BRIGHTNESS = int(WS281X['led_brightness'])
    LED_FREQ_HZ = int(WS281X['led_freq_hz'])
    LED_DMA = int(WS281X['led_dma'])
    LED_INVERT = WS281X['led_invert']
    LED_CHANNEL = int(WS281X['led_channel'])
    if LED_INVERT == 'True':
        LED_INVERT = True
    else:
        LED_INVERT = False
    led = WS281XController(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA,
                           LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    led.set_color(*DEFAULT_COLOR)
    print('WS281X Controller created')
else:
    print('WS281X is INACTIVE!')

print('Reading environment variables')
configenv = configparser.ConfigParser()
configenv.read('variables.ini')

ENV = configenv['ENV']
CLIENT_ID = ENV['CLIENT_ID']
print(CLIENT_ID)
CLIENT_SECRET = ENV['CLIENT_SECRET']
print(CLIENT_SECRET)
REDIRECT_URI = ENV['REDIRECT_URL']
print(REDIRECT_URI)
REFRESH_TOKEN = ENV['REFRESH_TOKEN']
print(REFRESH_TOKEN)

print('Creating spotify playback object')
spotify = CurrentSpotifyPlayback(CLIENT_ID, CLIENT_SECRET,
                                 REDIRECT_URI, REFRESH_TOKEN)

print('Created spotify playback')

name = config['CHROMECAST']['name']
print("Welcome back,", name, "!")

# purple = LedSetColorDelay(145, 36, 255, 1, led)
# blue = LedSetColorDelay(25, 68, 255, 1, led)
# redishPurple = LedSetColorDelay(255, 0, 123, 1, led)
# multi_color_mode = MultiColorMode()
# multi_color_mode.add_color(purple)
# multi_color_mode.add_color(blue)
# multi_color_mode.add_color(purple)
# multi_color_mode.add_color(redishPurple)
# executor = LedModeExecutor(multi_color_mode)

print("ELu")

process = Process(target=main_spotify, args=())

app.run(host='0.0.0.0', port=5000)

