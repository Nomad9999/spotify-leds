from abc import ABC, abstractmethod
from time import sleep


class LedCommand(ABC):

    @abstractmethod
    def execute(self):
        pass
