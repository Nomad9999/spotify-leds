from abc import ABC, abstractmethod
from time import sleep


class LedMode(ABC):

    @abstractmethod
    def run_mode(self):
        pass
