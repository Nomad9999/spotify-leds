from time import sleep

from led_command import LedCommand
from led_controller import LEDController
from led_mode import LedMode


class LedModeExecutor:

    def __init__(self, led_mode: LedMode):
        self.led_mode = led_mode

    def execute(self):
        self.led_mode.run_mode()

    def change_mode(self, led_mode: LedMode):
        self.led_mode = led_mode
        self.execute()