from time import sleep

from led_command import LedCommand
from led_mode import LedMode
from set_color_action import LedSingleColor


class MultiColorMode(LedMode):

    def __init__(self):
        self.colors = []

    def add_color(self, action: LedSingleColor):
        self.colors.append(action)

    def run_mode(self):
        for color in self.colors:
            color.execute()
