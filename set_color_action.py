from led_command import LedCommand
from ws281x_controller import WS281XController


class LedSingleColor(LedCommand):

    def __init__(self, r, g, b, led_controller: WS281XController):
        self.led_controller = led_controller
        self.r = r
        self.g = g
        self.b = b

    def execute(self):
        self.led_controller.set_color(self.r, self.g, self.b)
