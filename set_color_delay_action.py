from set_color_action import LedSingleColor
from ws281x_controller import WS281XController


class LedSetColorDelay(LedSingleColor):

    def __init__(self, r, g, b, delay, led_controller: WS281XController):
        super().__init__(r, g, b, led_controller)
        self.delay = delay

    def execute(self):
        self.led_controller.set_color(self.r, self.g, self.b, self.delay)
