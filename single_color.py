from time import sleep

from led_command import LedCommand
from led_mode import LedMode


class SingleColorMode(LedMode):

    def __init__(self, action: LedCommand):
        self.single_color_action = action

    def set_new_color(self, action: LedCommand):
        self.single_color_action = action

    def run_mode(self):
        self.single_color_action.execute()


